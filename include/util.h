
#ifndef UTIL_H
#define UTIL_H


/*
Funcion que dibuja una grilla en la terminal.
Recibe un arreglo 2D de chars. Si el elemento es el 
numero 0, se muestra un espacio vacio, si es 1, se 
muestra una x.

Cada llamada de esta funcion borrara todo lo mostrado
en pantalla.

(util.c)
*/
void dibujar_grilla(char **matriz, int fil, int col);

/*
Llena matriz con cantidad de 1s en posiciones al azar,
el resto lo llena de 0s. (util.c)
*/
void llenar_matriz_azar(char **grilla, int fil, int col, int cantidad);

/*
Crea y retorna la matriz 2D (tablero.c)
*/
char ** creaTablero(int filas, int columnas);

/*
Crea y retorna una nueva matriz 2D (despues de haber aplicado la reglas mencionadas)
(reglamento.c)
*/
char ** aplicaReglas(char **tablero, int filas, int columnas);

/*
Retorna la cantidad de celulas vivas en el tablero (operaciones.c)
*/
int vivasxgeneracion(char **tablero, int filas, int columnas);

/*
Retorna la cantidad de celulas muertas en el tablero (operaciones.c)
*/
int muertasxgeneracion(char **tablero, int filas, int columnas);

/*
Libera el espacio de memoria de la matriz anterior (memoria.c)
*/
void liberaEspacio(char **tablero, int filas, int columnas);


#endif 
