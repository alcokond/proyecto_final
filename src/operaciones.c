#include <stdio.h>

int vivasxgeneracion(char **tablero, int filas, int columnas){
    int vivas = 0;
    for(int f=0; f<filas; f++){
        for(int c=0; c<columnas; c++){
			if(tablero[f][c] == 1){
			    vivas+=1;
		    }
		}
	}

    return vivas;
}

int muertasxgeneracion(char **tablero, int filas, int columnas){ 
    int muertas = 0;

	for(int f=0; f<filas; f++){
        for(int c=0; c<columnas; c++){
			if(tablero[f][c] == 0){
			    muertas+=1;
		    }
		}
	}

    return muertas;
}
