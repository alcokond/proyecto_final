#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "util.h"
#include "vida.h"
//limite cuando f y c son iguales es 75000

int main(int argc, char *argv[]){

    //Informacion de la Estructura. Inician en -1 para validar el ingreso de informacion de manera correcta
    long filas = -1, columnas = -1, tiempo = -1, generaciones = -1, celulas_iniciales = -1;

    //Variables del Juego
    int es_infinito = 0, contador_generaciones = 0, total_vivas = 0, total_muertas = 0, generacion_vivas = 0, generacion_muertas = 0;

    //Solicita datos para crear estructura juego_de_vida
    int opt;
    while((opt=getopt(argc,argv,"f:c:g:s:i:")) != -1){
        switch(opt){
            case 'f':
                filas = atol(optarg);
                break;
            case 'c':
                columnas = atol(optarg);
                break;
            case 'g':
                generaciones = atol(optarg);
                break;
            case 's':
                tiempo = atol(optarg);
                break;
            case 'i':
                celulas_iniciales = atol(optarg);
                break;
            default:
                printf("Opcion no encotrada\n");
                break;
        }
    }

    if(filas>0 && columnas>0 && tiempo>0 && celulas_iniciales>0 && (filas*columnas)<=2500000){ //Valida que el usuario ingreso datos
        //Crea el tablero con las filas y columnas indicadas
        char **tablero = creaTablero(filas,columnas);

        //Crea estructura juego_de_vida
        juego_de_vida juego;
        juego.tablero = tablero;
        juego.filas = filas;
        juego.columnas = columnas;
        juego.tiempo_sleep = tiempo;
        juego.generaciones = generaciones;

        //Llena de 1s y 0s el tablero
        llenar_matriz_azar(juego.tablero,filas,columnas,celulas_iniciales);

        //Dibuja el tablero con espacios y x
        dibujar_grilla(juego.tablero, filas, columnas);

        //Imprime informacion de la Generacion 0
        generacion_vivas += vivasxgeneracion(juego.tablero, filas, columnas);
        generacion_muertas += muertasxgeneracion(juego.tablero, filas, columnas);
        total_vivas += generacion_vivas;
        total_muertas += generacion_muertas;
        
        printf("\nGeneración %d\n", contador_generaciones);
        printf("\tNúmero de células vivas en esta generación: %d\n", generacion_vivas);
        printf("\tNúmero de células muertas en esta generación: %d\n", generacion_muertas);
        printf("\tNúmero de células vivas desde generación 0: %d\n", total_vivas);
        printf("\tNúmero de células muertas desde generación 0: %d\n", total_muertas);
        printf("\tPromedio de células que murieron por generación: %.2f\n", total_muertas/(float)generaciones);
        printf("\tPromedio de células que nacieron por generación: %.2f\n", total_vivas/(float)generaciones);
        printf("\n");
        printf("\n");

        if((int)generaciones <= 0){
            es_infinito = 1;
        }

        //Inicia Juego de Vida
        while(contador_generaciones<(int)generaciones-1 || es_infinito == 1){
            //Para sumar solo los de esa Generacion
            generacion_vivas = 0;
            generacion_muertas = 0;

            //Nuevo tablero con reglas aplicadas
            char **nuevo_tablero = aplicaReglas(tablero, filas, columnas);

            //Libera memoria del tablero anterior
            liberaEspacio(juego.tablero, filas, columnas);

            //Asigna el nuevo tablero a la estructura y actualiza generacion CONSULTAR ESTO
            juego.tablero = nuevo_tablero;
            juego.generaciones = contador_generaciones+1;

            //Imprime el tablero de la Generacion
            dibujar_grilla(nuevo_tablero, filas, columnas);

            //Imprime informacion de la Generacion
            generacion_vivas += vivasxgeneracion(juego.tablero, filas, columnas);
            generacion_muertas += muertasxgeneracion(juego.tablero, filas, columnas);
            total_vivas += generacion_vivas;
            total_muertas += generacion_muertas;

            printf("Generación %d\n", contador_generaciones+1);
            printf("\tNúmero de células vivas en esta generación: %d\n", generacion_vivas);
            printf("\tNúmero de células muertas en esta generación: %d\n", generacion_muertas);
            printf("\tNúmero de células vivas desde generación 0: %d\n", total_vivas);
            printf("\tNúmero de células muertas desde generación 0: %d\n", total_muertas);
            printf("\tPromedio de células que murieron por generación: %.2f\n", total_muertas/(float)generaciones);
            printf("\tPromedio de células que nacieron por generación: %.2f\n", total_vivas/(float)generaciones);
            printf("\n");
            printf("\n");

            //Cuento la generacion
            contador_generaciones+=1;

            //Tiempo para volver a mostrar
            usleep(juego.tiempo_sleep);
        }
    }else{
        printf("No se pudo iniciar el Juego de Vida.\n");
        printf("Recuerda:\n");
        printf("1. No puedes jugar sin haber ingresado valores.\n");
        printf("2. Los valores ingresados deben ser mayores a cero.\n");
        printf("3. La multiplicacion entre filas y columnas no debe ser mayor a 2500000.\n");
        printf("NOTA: La ultima opcion es para evitar que el programa muera a pesar de cumplir con las 2 primeras condiciones.\n");
    }

    return 0;
}
