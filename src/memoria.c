#include <stdio.h>
#include <stdlib.h>

void liberaEspacio(char **tablero, int filas, int columnas){
    for(int i = 0; i < filas; i++){
        free(*(tablero+i));
    }
    free(tablero);
}