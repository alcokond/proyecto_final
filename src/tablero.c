#include <stdio.h>
#include <stdlib.h>

char ** creaTablero(int filas, int columnas){
    char **tablero = (char **)malloc(filas*sizeof(char *)); //creacion de todas las filas

    for(int i=0; i<filas; i++){
        *(tablero+i) = (char *)malloc(columnas*sizeof(char)); //creacion de todas las columnas
    }
    return tablero;
}

