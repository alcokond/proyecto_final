#include "util.h"
#include <stdio.h>

char ** aplicaReglas(char **tablero, int filas, int columnas){
    char **nuevo_tablero = creaTablero(filas,columnas);
    int vecinos;
    for(int f=0; f<filas; f++){
        for(int c=0; c<columnas; c++){
            vecinos = 0;
            
            if(f==0){
                
                if(c==0){
                    vecinos =  vecinos + tablero[f][c+1] + tablero[f+1][c] + tablero[f+1][c+1];
                }else if(c==columnas-1){
                    vecinos =  vecinos +  tablero[f][c-1] + tablero[f+1][c-1] + tablero[f+1][c];
                }else{
                    vecinos =  vecinos +  tablero[f][c-1] + tablero[f][c+1] + tablero[f+1][c-1] + tablero[f+1][c] + tablero[f+1][c+1]; 
                }

            }
            
            else if(f==filas-1){

                if(c==0){
                    vecinos =  vecinos + tablero[f][c+1] + tablero[f-1][c] + tablero[f-1][c+1];
                }else if(c==columnas-1){
                    vecinos =  vecinos +  tablero[f][c-1] + tablero[f-1][c] + tablero[f-1][c-1];
                }else{
                    vecinos =  vecinos + tablero[f][c-1] + tablero[f][c+1] + tablero[f-1][c] + tablero[f-1][c-1] + tablero[f-1][c+1]; 
                }

            }else if(c==0 && f>0 && f<filas-1){
                vecinos =  vecinos + tablero[f][c+1] + tablero[f-1][c] + tablero[f-1][c+1] + tablero[f+1][c] + tablero[f+1][c+1]; 
            }else if(c==columnas-1 && f>0 && f<filas-1){
                vecinos =  vecinos + tablero[f][c-1] + tablero[f+1][c] + tablero[f+1][c-1] + tablero[f-1][c] + tablero[f-1][c-1]; 
            }else{
                vecinos =  vecinos + tablero[f][c-1] + tablero[f][c+1] + tablero[f+1][c-1] + tablero[f+1][c] + tablero[f+1][c+1] + tablero[f-1][c-1] + tablero[f-1][c+1] + tablero[f-1][c]; 
            }


            if(tablero[f][c]==1){
                if(vecinos<=1 || vecinos>=4){
                    nuevo_tablero[f][c] = 0;
                }else{
                    nuevo_tablero[f][c] = 1;
                }
            }else{
                if(vecinos==3){
                    nuevo_tablero[f][c] = 1;
                }else{
                    nuevo_tablero[f][c] = 0;
                }
            }
        }
    }

    return nuevo_tablero;
}
