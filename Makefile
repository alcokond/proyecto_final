bin/vida: obj/util.o obj/tablero.o  obj/reglamento.o obj/operaciones.o obj/memoria.o obj/main.o
	gcc obj/util.o obj/tablero.o obj/reglamento.o obj/operaciones.o obj/memoria.o obj/main.o -o bin/vida

obj/util.o: src/util.c
	gcc -Wall -c src/util.c -o obj/util.o

obj/tablero.o: src/tablero.c
	gcc -Wall -c src/tablero.c -o obj/tablero.o

obj/reglamento.o: src/reglamento.c
	gcc -Wall -c -I include src/reglamento.c -o obj/reglamento.o

obj/operaciones.o: src/operaciones.c
	gcc -Wall -c src/operaciones.c -o obj/operaciones.o

obj/memoria.o: src/memoria.c
	gcc -Wall -c src/memoria.c -o obj/memoria.o

obj/main.o: src/main.c
	gcc -Wall -c -I include src/main.c -o obj/main.o

.PHONY: clean
clean:
	rm bin/* obj/*
